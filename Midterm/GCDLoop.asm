.data
str1: .asciiz "Enter an integer: "
str2: .asciiz "\nEnter another integer : "
str3: .asciiz "The greatest common divisor is "

.text
main:

la $a0, str1 
li $v0, 4 
syscall 

li $v0, 5 
syscall 
add $a1, $v0, $zero 

la $a0, str2 
li $v0, 4 
syscall 

li $v0, 5 
syscall 
add $a2, $v0, $zero 

addi $sp, $sp, -8 
sw $a1, 4($sp) 
sw $a2, 0($sp) 
jal gcdLoop 

la $a0, str3 
li $v0, 4 
syscall
li $v0, 1 
add $a0, $s0, $zero 
syscall

gcdLoop:

move $t0, $a0
move $t1, $a1
lw $ra, 4($sp)        
addi $sp, $sp, 8      
jr $ra               

loop:
beq $t1, $0, done
div $t0, $t1
move $t0, $t1
mfhi $t1
j loop
done:
move $v0, $t0
jr $ra
